/*!
 * Project: thingsboard.io
 * File:    ./config/index.js
 * Created: 2017-12-29
 */

'use strict';

/**
 * DEPENDENCIES
 * @private
 */

const path  = require('path');
const nconf = require('nconf');


/**
 * DECLARATION
 * @void
 */

const appDir = path.dirname(require.main.filename);
const dir = nconf.get('dir');

nconf.argv().env({separator: '__'});
nconf.file('local', path.join(__dirname, 'config.local.json'));
nconf.file(path.join(__dirname, 'config.json'));

//  Added flag --test-enabled to launch application in test environment
nconf.argv({
  'test:enabled': {
    alias:    'test-enabled',
    describe: 'Launch application with --test flag enabled to set test environment. It used during testing process',
    demand:   false,
    type:     'boolean',
    default:  false
  }
});

//  use test environment if it is launched from mocha or with --test-enabled
if (nconf.get('test:enabled') || (-1 !== process.argv[1].indexOf('mocha'))) {
  nconf.file('local', path.join(__dirname, 'config.test.local.json'));
  nconf.file(path.join(__dirname, 'config.test.json'));
}

//  Calculate absolute path for directories
for (var i in dir) {
  if (dir.hasOwnProperty(i)) {
    nconf.set('dir:' + i, path.join(appDir, dir[i]));
  }
}


/**
 * EXPOSE
 * @public
 */

module.exports = exports = nconf;
